import sys

if __name__ == "__main__":
    if len(sys.argv) != 4:
        print "python "+sys.argv[0]+" infile.bp.txt infile.max.txt outfile.max.txt"
        sys.exit(0)
    outf = open(sys.argv[3],"w")
    bps = {}
    with open(sys.argv[1],"r") as fl:
        count = 0
        for f in fl:
            s = f.strip()
            bps[s] = count
            count += 1
    with open(sys.argv[2],"r") as fl:
        for f in fl:
            spls = f.split(" ")
            outf.write("b"+str(bps[" ".join(spls[0:-1])])+"\t"+spls[-1])
    outf.close()
