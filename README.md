# README #

What is this repository for?

This repo is predominantly for the conflict analysis commands, the rest has been consolidated to a one script called MGWE_Calc.pl at https://github.com/jfwalker/SiteSpecificLogLikelihood

This is a repo designed to accompany the Manuscript "Analyzing contentious nodes and outlier genes in phylogenomics"

This is a revision to the paper "Site and gene-wise likelihoods unmask influential outliers in phylogenomic analyses"

The goal of this is to develop a new way of looking at and analyzing contentious nodes and here we expand upon the two topology test by marginalizing the likelihoods using all unique topologies found among a set of trees. In the paper we use the set of trees that is from the inferred gene trees.

To run this process, you will need both RAxML and Phyx installed.

RAxML: https://sco.h-its.org/exelixis/web/software/raxml/index.html

Phyx: https://github.com/FePhyFoFum/phyx
Example of Marginalized Likelihoods

This example goes through the Chiari et. al 2012 dataset, I added one tree into the folder just so all the trees are not unique, because narrowing them down to unique trees is important for saving time. At the top it contains the species tree, then the tree below it is the alternative tree, then the trees below that are the ones from the gene trees that contain all species. To properly marginalize the likelihoods, all species are needed so the likelihood calculation can be performed across numerous topologies.

First go to the ExampleOfMarginalizedLikelihoods folder

Then run the Phyx program pxbp on the Example gene trees to make sure you only have unique trees

If running this on your own data make sure the first tree is the species tree

```pxbp -t ExampleAllGeneTrees.tre -u > Unique.tre```

You'll need to remove everything but the trees from the output, usually first 2 and last line

Then run pxbp to get the unique clades

```pxbp -t Unique.tre -e -v > bp.log```

Then you'll need to create the SSLL output file and this can be done with RAxML

```raxmlHPC-PTHREADS -f g -T 3 -s ExampleConcat.fa -q ExampleGenes.model -m GTRGAMMA -z Unique.tre -n EX_SSLL```

This output will then need to be condensed to genes

```perl Programs/GenesFromSites.pl ExampleGenes.model RAxML_perSiteLLs.EX_SSLL > GeneWise.txt```

Then you'll need to do a quick edit to the GeneWise.txt file, at the top it will say the number of trees (7)
and the number of sites (187026), the sites should be changed to number of genes 248.
 

 Then you'll need to get the best likelihood for each bipartition

+```python Programs/process_likes_splits_sites.py Unique.tre GeneWise.txt bp.log```

This will give you 3 files for each node.

    *.bp.txt is the different bipartitions at the given node

    *.sc.txt is the different likelihoods at each node

    *.max.txt is the bipartition that corresponds to the best likelihood for each node


Then we'll want to find the maximum one for the relationship of interest (fl10), in this case position of turtles

```python Programs/convert_max.py fl10.bp.txt fl10.max.txt BestGeneWise.txt```

The file BestGeneWise.txt will then contain the top genewise likelihood.

This same analysis can be done for getting the maximized site likelihoods, the difference is you would not
codense by gene and just run process_likes_splits.py on the RAxML file that has not been condensed


Other analyses and code to conduct them from the paper
Obtaining data

wget http://datadryad.org/bitstream/handle/10255/dryad.107733/alignments.zip?sequence=1

mv alignments.zip\?sequence=1 alignments.zip

unzip alignments.zip

Made trees for each topology from Chiari = BrownTrees.tre

pxcat -s *.nex -p Genes.model -o Concatenated_Chiari.fa
Run SSLL

raxmlHPC-PTHREADS-AVX2 -f g -T 2 -s Concatenated_Chiari.fa-q Genes.model -m GTRGAMMA -z BrownTrees.tre -n SSLL

Invert the data to make it easier for R

Remove Header

cp RAxML_perSiteLLs.SSLL NoHeader.txt

perl Switch_Row_Col.pl NoHeader.txt > Inverted.txt

Get the gene start and stop combined

perl RcodeForGenes.pl Genes.model > GeneStartStop.txt

Open R code SSLL_code.R
Make individual gene trees

for x in *.nex;do pxs2fa -s $x -o $x.fa;done

for x in *.fa;do perl ChangeFastaName.pl $x;done

for x in *.fa;do raxml -T 36 -p 12345 -m GTRCAT -s $x -n $x.tre;done

Format for Sh Test

Basic Regex in perl to change the names

for x in *.tre;do perl Rename.pl $x;done

231 of the 248 trees ran some trees only had missing data for taxa

Move all complete ones to a separate folder

for x in *.tre;do perl Rename.pl $x;done

Remove the taxa that are made up of only missing data

for x in *.fa;do raxml -T 36 -p 12345 -m GTRCAT -s $x -n $x.tre;done

Run Shtest

for x in *.fa;do raxml -T 9 -f J -p 12345 -m GTRGAMMA -s $x -t $x.tre -n $x.SH;done

Rename the RaxML output

Adjust the Sh

for x in *;do python ../../../../CarnivoryReanalyze/Programs/move_notes_labels_trees_raxml.py $x > $x.sh;done

reroot

for x in *.sh;do pxrr -t $x -g protopterus -o $x.rr;done

java -jar phyparts-0.0.1-SNAPSHOT-jar-with-dependencies.jar -m GeneTrees/HasOutgroup/ -d SpeciesTreeTurt.tre -s 80 -v -a 1

head out.concon.tre -n 1 > Concordant.tre

head out.concon.tre -n 2 | tail -n 1 > Conflict.tre

Make pie charts: python phypartspiecharts.py SpeciesTreeTurt.tre Vertebrate/out 93 --svg_name test.svg
Run SSLL on a single gene to remake Brown and Thompson result

Removed: protopterus,podarcis,alligator,python

Renamed 8916_Tre.tre

raxml -f g -T 2 -s ENSGALG00000008916.fa -m GTRGAMMA -z 8916_Tre.tre -n 8916

Tree 0: -3365.466682

Tree 1: -3447.758668
Run SSLL on a single gene to remake Brown and Thompson result

Removed: protopterus,podarcis,python,caiman,chelonoidis_nigra,emys_orbicularis

pxrmt -t BrownTrees.tre -n protopterus,podarcis,python,caiman,chelonoidis_nigra,emys_orbicularis -o 1362_Tre.tre

raxml -f g -T 2 -s ENSGALG00000001362.fa -m GTRGAMMA -z 1362_Tre.tre -n 1362

Tree 0: -2788.028501

Tree 1: -2807.699611

Make Species Trees

raxml -T 9 -f a -# 200 -m GTRCAT -p 12345 -x 112233 -s Concatenated_Chiari.fa -q Genes.model -n All

Remove 8916, 11434

raxml -T 9 -f a -# 200 -m GTRCAT -p 12345 -x 112233 -s Miss2.fa -q Miss2Genes.model -n Miss2

Results of Species Tree show those two genes alter the topology
Find species bipartition

Bip 0: grep "Xenopus" * | grep "Homo" | grep "Monodelphis" | grep "Ornithorhynchus" | grep "Gallus" | grep "Taeniopygia" | grep "alligator" | grep "caiman" | grep "phrynops" |grep "caretta" | grep "emys_orbicularis" | grep "chelonoidis_nigra" | grep "podarcis" | grep "python" | grep "Anolis" | wc -l

Bip 1: grep "Homo" * | grep "Monodelphis" | grep "Ornithorhynchus" | grep "Gallus" | grep "Taeniopygia" | grep "alligator" | grep "caiman" | grep "phrynops" |grep "caretta" | grep "emys_orbicularis" | grep "chelonoidis_nigra" | grep "podarcis" | grep "python" | grep "Anolis" | wc -l

Bip 2: grep "Homo" * | grep "Monodelphis" | grep "Ornithorhynchus" | wc -l

Bip 3: grep "Homo" * | grep "Monodelphis" | wc -l

Bip 4: grep "Gallus" * | grep "Taeniopygia" | grep "alligator" | grep "caiman" | grep "phrynops" |grep "caretta" | grep "emys_orbicularis" | grep "chelonoidis_nigra" | grep "podarcis" | grep "python" | grep "Anolis" | wc -l

Bip 5: grep "Gallus" * | grep "Taeniopygia" | grep "alligator" | grep "caiman" | grep "phrynops" |grep "caretta" | grep "emys_orbicularis" | grep "chelonoidis_nigra"| wc -l

Bip 6: grep "Gallus" * | grep "Taeniopygia" | wc -l

Bip 7: grep "alligator" * | grep "caiman" | grep "phrynops" |grep "caretta" | grep "emys_orbicularis" | grep "chelonoidis_nigra" | wc -l

Bip 8: grep "alligator" * | grep "caiman" | wc -l

Bip 9: grep "phrynops" *|grep "caretta" | grep "emys_orbicularis" | grep "chelonoidis_nigra"| wc -l

Bip 10: grep "caretta" * | grep "emys_orbicularis" | grep "chelonoidis_nigra"| wc -l

Bip 11: grep "emys_orbicularis" * | grep "chelonoidis_nigra"| wc -l

Bip 12: grep "podarcis" * | grep "python" | grep "Anolis" | wc -l

Bip 13: grep "python" * | grep "Anolis" | wc -l

Alt Bip: grep "Gallus" * | grep "Taeniopygia" | grep "phrynops" |grep "caretta" | grep "emys_orbicularis" | grep "chelonoidis_nigra" | wc -l
Contributors

    Joseph F. Walker

    Joseph W. Brown

    Stephen A. Smith

Questions

    Contact: jfwalker@umich.edu eebsmith@umich.edu
