library(ggplot2)
library(cowplot)
library(gridExtra)
rm(list = ls())

#For Two Topology Vertebrate
setwd("/home/jfwalker/Desktop/SSLL/marginalizelikelihoods/ForDryad/GeneWiseLikelihoods/SiteCounts/")
TwoTopV = read.table("TwoTopVert.txt",skip=0,sep= "\t")
TwoTopV$V1 <- factor(TwoTopV$V1, levels =c("S","C"))
TTV <- ggplot(TwoTopV,aes(V1,V2)) + geom_bar(stat="identity")
TTV <- TTV + xlab("Topology") + ylab("Gene Count") + ggtitle("Two Topo Vert.")

#Gene Wise Vertebrate
setwd("/home/jfwalker/Desktop/SSLL/marginalizelikelihoods/ForDryad/GeneWiseLikelihoods/SiteCounts/")
GeneV = read.table("GeneWiseVert.txt",skip=0,sep= "\t")
GeneV$V1 <- factor(GeneV$V1, levels =c("S","C","DA"))
GV <- ggplot(GeneV,aes(V1,V2)) + geom_bar(stat="identity")
GV <-GV + xlab("Bipartition") + ylab("Gene Count") + ggtitle("MGWB Vert.")


#For Marginalized Vertebrate
setwd("/home/jfwalker/Desktop/SSLL/marginalizelikelihoods/ForDryad/GeneWiseLikelihoods/SiteCounts/")
MargV = read.table("MarginalizedVertebrateSiteCount.txt",skip=0,sep= "\t")
MargV$V1 <- factor(MargV$V1, levels =c("S","C","DA"))
MV <- ggplot(MargV,aes(V1,V2)) + geom_bar(stat="identity")
MV <- MV + xlab("Bipartition") + ylab("Site Count") + ggtitle("MSWB Vert.")


#For Two Topology Carnivory
setwd("/home/jfwalker/Desktop/SSLL/marginalizelikelihoods/ForDryad/GeneWiseLikelihoods/SiteCounts/")
TwoTopC = read.table("TwoTopCarnivory.txt",skip=0,sep= "\t")
TwoTopC$V1 <- factor(TwoTopC$V1, levels =c("S","C"))
TTC <- ggplot(TwoTopC,aes(V1,V2)) + geom_bar(stat="identity")
TTC <- TTC + xlab("Topology") + ylab("Gene Count") + ggtitle("Two-Topo Carn.")


#Gene Wise Carnivory
setwd("/home/jfwalker/Desktop/SSLL/marginalizelikelihoods/ForDryad/GeneWiseLikelihoods/SiteCounts/")
GeneC = read.table("GeneWiseCarnivoryForGraph.txt",skip=0,sep= "\t")
GeneC$V1 <- factor(GeneC$V1, levels =c("S","C","DA"))
GC <- ggplot(GeneC,aes(V1,V2)) + geom_bar(stat="identity")
GC <-GC + xlab("Bipartition") + ylab("Gene Count") + ggtitle("MGWB Carn.")

#For Marginalized Carnivory
setwd("/home/jfwalker/Desktop/SSLL/marginalizelikelihoods/ForDryad/GeneWiseLikelihoods/SiteCounts/")
MargC = read.table("MarginalizedCarnivorySiteCountForGraph.txt",skip=0,sep= "\t")
MargC$V1 <- factor(MargC$V1, levels =c("S","C","DA"))
MC <- ggplot(MargC,aes(V1,V2)) + geom_bar(stat="identity")
MC <- MC + xlab("Bipartition") + ylab("Site Count") + ggtitle("MSWB Carn.")

plot_grid(TTV,GV,MV,TTC,GC,MC,labels=c("A","B","C","D","E","F"),ncol=3,nrow=2)
